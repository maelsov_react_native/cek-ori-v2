import { Dimensions, Platform } from "react-native"
import { getStatusBarHeight } from 'react-native-status-bar-height'

export const Fonts = {
    SF_COMPACT_REGULAR:'SFCompactText-Regular',
    SF_COMPACT_MEDIUM:'SFCompactText-Medium',
    SF_COMPACT_SEMI_BOLD:'SFCompactText-SemiBold',
    SF_COMPACT_BOLD:'SFCompactText-Bold',
    SF_COMPACT_BOLD_ITALIC:'SFCompactText-BoldItalic',
}

export const Colors = {
    RED_DARK: "#5e0003",
    RED: '#910005',
    GREEN: '#3A8048',
    BLACK: '#000000',
    GRAY: '#a7a9ab',
    GRAY_LIGHT: '#efefef',
    WHITE: '#ffffff',
    BLUE : '#0691ce'
}

export const Metrics = Object.freeze({
    SAFE_AREA: 16,
    STATUSBAR_HEIGHT : getStatusBarHeight(false),
    SCREEN_WIDTH: Dimensions.get('window').width,
    SCREEN_HEIGHT: Dimensions.get('window').height,
    NAVBAR_HEIGHT: 56
})

export const WarningMessage = {
    ErrorMessage: 'Koneksi tidak tersedia'
}

export const StorageKey = {
    Language: 'language',
}

export const Images = {
    LOGO: require("./images/logo.png"),
}

export const ArrayOfLanguage = [
    {
        languageName: 'Bahasa Indonesia',
        code: 'ID'
    },
    {
        languageName: 'English',
        code: 'EN'
    },
]


export const TSVer = Platform.OS == 'android' ? '1' : '1'
export const PPVer = Platform.OS == 'android' ? '1' : '1'
export const Version = Platform.OS == 'android' ? '2.0.0' : '2.0.0'

export const StoreURL = Platform.OS == 'android' ? StoreURLANDROID : StoreURLIOS
export const StoreURLIOS = 'https://apps.apple.com/us/app/cekori/id1478556984'
export const StoreURLANDROID = 'https://play.google.com/store/apps/details?id=com.cekoriv2'