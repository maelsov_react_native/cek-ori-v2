import React, { useEffect, useState } from 'react';
import { View, TouchableOpacity, Linking, Image, Text, StyleSheet, ScrollView } from 'react-native';
import { Colors, Fonts, Metrics, Images, StoreURLANDROID, StoreURLIOS } from '../../GlobalConfig'
import { connect } from 'react-redux';
import languageInfo from './languageInfo';
import { Actions } from 'react-native-router-flux';

const InfoScreen = (props) => {
    const { activeLanguage } = props
    const [isLoading, setIsLoading] = useState(false)
    const [platform, setPlatform] = useState("")
    useEffect(() => {
        if (Platform.OS == 'android') {
            setPlatform(StoreURLANDROID)
        }
        else {
            setPlatform(StoreURLIOS)
        }

    }, [])
    const changeLanguage = () => {
        setIsLoading(true)
        Actions.multiOption({
            chosenType: 'language'
        })
        setIsLoading(false)
    }

    return (
        <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>
            <View style={{ position: 'absolute', top: 40, right: 24 }}>
                <TouchableOpacity
                    disabled={isLoading}
                    onPress={changeLanguage}
                    style={[
                        Platform.OS == 'ios' && { paddingTop: 4, paddingLeft: 1 },
                        { height: 36, width: 36, justifyContent: 'center', alignItems: 'center', borderRadius: 18, borderWidth: 2, borderColor: Colors.RED, backgroundColor: Colors.WHITE }
                    ]}>
                    <Text style={{ fontFamily: Fonts.SF_COMPACT_BOLD, fontSize: 14, letterSpacing: 0.5, color: Colors.RED, textAlign: 'center' }}>{activeLanguage}</Text>
                </TouchableOpacity>
            </View>
            <View style={{ marginTop: Metrics.SAFE_AREA, height: 150, justifyContent: 'center' }}>
                <Image resizeMode="contain" source={Images.LOGO} style={{ width: 200, height: 180, tintColor: Colors.RED, alignSelf: 'center' }} />
            </View>
            <ScrollView>
            <View style={{marginVertical: Metrics.SAFE_AREA, width: '100%', alignItems: 'center', paddingHorizontal: 20 }}>
                <Text style={{ fontFamily: Fonts.SF_COMPACT_BOLD, fontSize: 24, color: Colors.BLACK, textAlign: 'center', marginBottom:36}}>{languageInfo[activeLanguage].TEXT_HEADER}</Text>
                <Text style={{ fontFamily: Fonts.SF_COMPACT_REGULAR, fontSize: 20, color: Colors.BLACK, textAlign: 'center', marginBottom:36}}>{languageInfo[activeLanguage].TEXT_HEADER_2}</Text>
                <Text style={{ fontFamily: Fonts.SF_COMPACT_REGULAR, fontSize: 20, color: Colors.BLACK, textAlign: 'center' }}>{languageInfo[activeLanguage].TEXT_HEADER_3}</Text>
            </View>
            </ScrollView>
            <View style={styles.bottom}>
                <TouchableOpacity
                    onPress={() => Linking.openURL(platform)}
                    style={
                        {
                            backgroundColor: Colors.RED,
                            alignSelf: 'center',
                            height: Metrics.SCREEN_HEIGHT * 0.08,
                            width: Metrics.SCREEN_WIDTH * 0.4,
                            borderRadius: 40,
                            alignItems: 'center',
                            justifyContent: 'center'
                        }
                    }>
                    <Text style={{ fontFamily: Fonts.SF_COMPACT_MEDIUM, color: '#fff', fontSize: 18, textAlign: 'center' }}>{languageInfo[activeLanguage].BUTTON_DOWNLOAD}</Text>
                </TouchableOpacity>
            </View>
        </View>
    );
}
const styles = StyleSheet.create({
    bottom: {
        width: '100%',
        height: 100,
        paddingHorizontal: 30,
        justifyContent: 'center',
    },
})

const mapStateToProps = state => {
    return {
        activeLanguage: state.languageOperation.activeLanguage
    };
};

export default connect(mapStateToProps)(InfoScreen);