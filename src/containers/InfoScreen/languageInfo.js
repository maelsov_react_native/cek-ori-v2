export default {
    EN: {
        TEXT_HEADER: "We would like to notify that there is already a newer version of the apps which are build from the ground up with new functionalities for you.",
        TEXT_HEADER_2: 'Please download the new version from the Play Store by clicking the button below.',
        TEXT_HEADER_3: 'Subsequently you can delete this older version since all your accounts and credentials are preserved to be used with the newer version.',

        BUTTON_DOWNLOAD : 'Download New Version'
    },
    ID: {
        TEXT_HEADER: 'Kami ingin memberi tahu bahwa sudah ada versi aplikasi terbaru yang dibuat dari awal dengan fungsi baru untuk Anda.',
        TEXT_HEADER_2: 'Silahkan unduh versi terbaru dari Play Store dengan menekan tombol dibawah.',
        TEXT_HEADER_3: 'Selanjutnya anda dapat menghapus versi lama ini karena semua akun dan kredensial telah disimpan untuk digunakan pada versi yang terbaru.',

        BUTTON_DOWNLOAD : 'Unduh Versi Terbaru'
    },
}