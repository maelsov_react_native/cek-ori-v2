import React, { useEffect } from 'react';
import {
	View,
	Image,
} from 'react-native';
import { Colors, StorageKey, Images } from '../GlobalConfig';
import { Actions } from 'react-native-router-flux';
import AsyncStorage from '@react-native-community/async-storage';
import { connect } from 'react-redux';

import { wait } from '../GlobalFunction';

import { changeLanguage } from '../redux/actions/actionTypes';

const SplashScreen = (props) => {
	const { activeLanguage } = props

	useEffect(() => {
		AsyncStorage.getItem(StorageKey.Language)
			.then(val => {
				if (val) props.changeLanguage(val)
			},
				err => console.warn(err)
			)
		wait(2000)
			.then(() => {
				Actions.info()
			})
	}, [])



	return (
		<View style={{ flex: 1, backgroundColor: Colors.RED, alignItems: "center", justifyContent: "center" }}>
			<Image resizeMethod="resize" fadeDuration={0} source={Images.LOGO} style={{ flex: 0.7, width: "80%", height: "auto", resizeMode: "contain" }} />
		</View>
	);
}

const mapStateToProps = state => {
	return {
		activeLanguage: state.languageOperation.activeLanguage
	};
};

const mapDispatchToProps = dispatch => {
	return {
		changeLanguage: language => {
			dispatch(changeLanguage(language));
		},
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(SplashScreen);