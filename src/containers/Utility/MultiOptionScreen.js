import React, { useEffect, useState } from 'react';
import { View, Text, FlatList, TouchableOpacity } from 'react-native';
import { Colors, Metrics, Fonts, ArrayOfPhonePrefix, ArrayOfLanguage, StorageKey } from '../../GlobalConfig';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { Actions } from 'react-native-router-flux';
import { wait } from '../../GlobalFunction';
import languageMultiOption from './languageMultiOption';
import { connect } from 'react-redux';
import { changeLanguage } from '../../redux/actions/actionTypes';
import AsyncStorage from '@react-native-community/async-storage';

const OptionItem = (props) => {
		let item = props.item
		return (
			<TouchableOpacity
				onPress={() => props.handleOptionPress(item)}
				style={[{
					height: 60,
					paddingHorizontal: 20,
					borderBottomWidth: 1,
					flexDirection: 'row',
					alignItems: 'center'
				}]}>
				<View style={{ flex: 1 }}>
					<Text style={{ fontFamily: Fonts.SF_COMPACT_MEDIUM }}>{props.label}</Text>
				</View>
				<View style={{ marginLeft: Metrics.SAFE_AREA }}>
					<Ionicons
						name={props.selected ? 'md-radio-button-on' : 'md-radio-button-off'}
						size={28}
						color={Colors.RED}
					/>
				</View>
			</TouchableOpacity>
		)
	}

const MultiOptionScreen = (props) => {

	const { chosenType, activeLanguage, activePrefix } = props
	const [selected, setSelected] = useState(null)
	const [optionList, setOptionList] = useState([])
	const [search, setSearch] = useState("")
	const [isLoading, setIsLoading] = useState(true)
	const [tempOptionList, setTempOptionList] = useState([])


	useEffect(() => {
		let title = ''
		switch (chosenType) {
			case 'phonePrefix':
				title = languageMultiOption[activeLanguage].TEXT_DIALING_CODE_HEADER
				setOptionList(ArrayOfPhonePrefix)
				setTempOptionList(ArrayOfPhonePrefix)
				setSelected(activePrefix !== null ? activePrefix : null)
				setIsLoading(false)
				break;
			case 'language':
				title = languageMultiOption[activeLanguage].TEXT_LANGUAGE_HEADER
				setOptionList(ArrayOfLanguage)
				setTempOptionList(ArrayOfLanguage)
				setSelected(activeLanguage)
				setIsLoading(false)
				break;
			default:
				break;
		}
		wait(0).then(() => {
			if (Actions.currentScene == 'multiOption') Actions.refresh({ title })
		})
	})

	const handleOptionPress = (item) => {
		Actions.pop()

		switch (chosenType) {
			case 'phonePrefix':
				const { prefix } = item

				wait(0).then(() => {
					Actions.refresh({
						lastUpdatePhonePrefix: new Date,
						activePrefix: prefix
					})
				})
				break;
			case 'language':
				const { languageName, code } = item
				AsyncStorage.setItem(StorageKey.Language, code)
				props.changeLanguage(code)
				break;
			default:
				break;
		}
	}

	
		return (
			<View style={{ flex: 1, backgroundColor: Colors.WHITE }}>
				<FlatList
					data={tempOptionList}
					extraData={[tempOptionList, selected]}
					renderItem={({ item, index }) => {
						let selectedOption = '', label = ''

						switch (chosenType) {
							case 'phonePrefix':
								label = `(${item.prefix})  ${item.countryName}`
								selectedOption = selected == item.prefix
								break;
							case 'language':
								label = `(${item.code})  ${item.languageName}`
								selectedOption = selected == item.code
								break;
							default:
								break;
						}

						return (
							<OptionItem
								item={item}
								index={index}
								label={label}
								selected={selectedOption}
								handleOptionPress={handleOptionPress} />
						)
					}}
					keyExtractor={(item, index) => {
						let key = ''

						switch (chosenType) {
							case 'phonePrefix':
								key = item.prefix
								break;
							case 'language':
								key = item.code
								break;
							default:
								break;
						}

						return String(key)
					}} />
			</View>
		);
	}

const mapStateToProps = state => {
	return {
		activeLanguage: state.languageOperation.activeLanguage
	};
};

const mapDispatchToProps = dispatch => {
	return {
		changeLanguage: language => {
			dispatch(changeLanguage(language));
		},
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(MultiOptionScreen);