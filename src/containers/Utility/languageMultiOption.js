export default {
    EN: {
        TEXT_DIALING_CODE_HEADER: 'Choose Dialing Code',
        TEXT_LANGUAGE_HEADER: 'CHOOSE LANGUAGE',
    },
    ID: {
        TEXT_DIALING_CODE_HEADER: 'Pilih Kode Panggilan',
        TEXT_LANGUAGE_HEADER: 'PILIH BAHASA',
    },
}